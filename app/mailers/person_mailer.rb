class PersonMailer < ApplicationMailer
  def send_email_user
    @email = params[:subject, :content, :to, :from]
    mail to: @email.to, subject: "#{@email.subject} -> Conteúdo: #{@email.content}"
  end
end
