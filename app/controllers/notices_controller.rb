class NoticesController < ApplicationController
  before_action :set_notice, only: [:show, :edit, :update, :destroy]

  def index
    if user_signed_in?
      if current_user.admin == false
        redirect_to feed_path
      end
    end
        
    @notices = Notice.all
  end

  def show
    if user_signed_in?
      if current_user.admin == false
        redirect_to feed_path
      end
    end    
  end

  def new
    if user_signed_in?
      if current_user.admin == false
        redirect_to feed_path
      end
    end

    @notice = Notice.new
  end

  def edit
    if user_signed_in?
      if current_user.admin == false
        redirect_to feed_path
      end
    end    
  end

  def create
    if user_signed_in?
      if current_user.admin == false
        redirect_to feed_path
      end
    end

    @notice = Notice.new(notice_params)

    respond_to do |format|
      if @notice.save
        format.html { redirect_to @notice, notice: 'Notice was successfully created.' }
        format.json { render :show, status: :created, location: @notice }
      else
        format.html { render :new }
        format.json { render json: @notice.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    if user_signed_in?
      if current_user.admin == false
        redirect_to feed_path
      end
    end

    respond_to do |format|
      if @notice.update(notice_params)
        format.html { redirect_to @notice, notice: 'Notice was successfully updated.' }
        format.json { render :show, status: :ok, location: @notice }
      else
        format.html { render :edit }
        format.json { render json: @notice.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    if user_signed_in?
      if current_user.admin == false
        redirect_to feed_path
      end
    end

    @notice.destroy
    respond_to do |format|
      format.html { redirect_to notices_url, notice: 'Notice was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    def set_notice
      @notice = Notice.find(params[:id])
    end

    def notice_params
      params.require(:notice).permit(:title, :description, :url, :image)
    end
end
