class MotivationsController < ApplicationController
  before_action :set_motivation, only: [:show, :edit, :update, :destroy]

  def index
    @motivations = Motivation.all
  end

  def show
  end

  def new
    @motivation = Motivation.new
  end

  # GET /motivations/1/edit
  def edit
  end

  def create
    @motivation = Motivation.new(motivation_params)

    respond_to do |format|
      if @motivation.save
        format.html { redirect_to @motivation, notice: 'Motivation was successfully created.' }
        format.json { render :show, status: :created, location: @motivation }
      else
        format.html { render :new }
        format.json { render json: @motivation.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @motivation.update(motivation_params)
        format.html { redirect_to @motivation, notice: 'Motivation was successfully updated.' }
        format.json { render :show, status: :ok, location: @motivation }
      else
        format.html { render :edit }
        format.json { render json: @motivation.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @motivation.destroy
    respond_to do |format|
      format.html { redirect_to motivations_url, notice: 'Motivation was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    def set_motivation
      @motivation = Motivation.find(params[:id])
    end

    def motivation_params
      params.require(:motivation).permit(:name, :description)
    end
end
