class PagesController < ApplicationController
  def dashboard
    if user_signed_in?
      redirect_to feed_path
    end
  end

  def feed
    @feed = true

    @publication = Publication.new
    @categories = Category.all

    @publications_by_follow = []

    current_user.users.each do |u|
      u.publications.each do |p|
        @publications_by_follow << p
      end
    end

    current_user.publications.each do |p|
      @publications_by_follow << p
    end
  end

  def geral
    @geral = true

    @categories_names = []
    @categories = Category.all

    Category.all.each do |c|
      @categories_names << c.name
    end

    @publication_before_ransack = Publication.all

    @q = @publication_before_ransack.ransack(params[:q])
    @publications = @q.result.page(params[:page])
  end

  def pessoas
    @pessoas = true
    
    @user_follower = false
    
    @report = Report.new

    @q = User.all.ransack(params[:q])
    @users = @q.result.page(params[:page])
  end
end