class ApplicationController < ActionController::Base
  before_action :configure_permitted_parameters, if: :devise_controller?
  before_action :authenticate_user!, except: [ :dashboard, :comunidade, :sobre ]
  
  layout :layout_by_resource

  def after_sign_in_path_for(resource)
    feed_path
  end

  protected

  def layout_by_resource
    if devise_controller?
      "layout_devise"
    else
      "layout_pages"
    end
  end

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [:image, :name, :age, :description, :phone, :address])
    devise_parameter_sanitizer.permit(:account_update, keys: [:image, :name, :age, :description, :phone, :address])
  end
end