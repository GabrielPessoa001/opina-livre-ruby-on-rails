class UsersController < ApplicationController
  before_action :set_user, only: [:show, :edit, :update]

  def add_new_follower
    redirect_to pessoas_path, notice: 'Você seguiu o usuário com sucesso' if current_user.users << User.all.select { |m| m.email == params[:user_email] }
  end

  def remote_follower
    redirect_to pessoas_path, notice: 'Você deixou de seguir o usuário especificado' if current_user.users.delete(User.all.select { |m| m.email == params[:user_email] })
  end

  def index
    if user_signed_in?
      if current_user.admin == false
        redirect_to feed_path
      end
    end

    @users = User.all
  end

  def show
    if user_signed_in?
      if current_user.admin == false
        redirect_to feed_path
      end
    end
  end

  def new
    if user_signed_in?
      if current_user.admin == false
        redirect_to feed_path
      end
    end

    @user = User.new
  end

  def edit
    if user_signed_in?
      if current_user.admin == false
        redirect_to feed_path
      end
    end
  end

  def create
    @user = User.new(user_params)

    respond_to do |format|
      if @user.save
        format.html { redirect_to @user, notice: 'User was successfully created.' }
        format.json { render :show, status: :created, location: @user }
      else
        format.html { render :new }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @user.update(user_params)
        format.html { redirect_to @user, notice: 'User was successfully updated.' }
        format.json { render :show, status: :ok, location: @user }
      else
        format.html { render :edit }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    if user_signed_in?
      if current_user.admin == false
        redirect_to feed_path
      end
    end
    
    @user.destroy
    respond_to do |format|
      format.html { redirect_to users_url, notice: 'User was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    def set_user
      @user = User.find(params[:id])
    end

    def user_params
      params.require(:user).permit(:name, :age, :description, :phone, :address, :admin, :image)
    end
end
