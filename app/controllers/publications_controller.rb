class PublicationsController < ApplicationController
  before_action :set_publication, only: [:show, :edit, :update, :destroy]

  def increment_like_geral
    @publication = Publication.all.find(params[:publication_like])

    @publication.like = @publication.like+1

    @publication.save

    redirect_to geral_path
  end

  def increment_deslike_geral
    @publication = Publication.all.find(params[:publication_deslike])

    @publication.deslike = @publication.deslike-1

    @publication.save

    redirect_to geral_path
  end

  def increment_like_feed
    @publication = Publication.all.find(params[:publication_like])

    @publication.like = @publication.like+1

    @publication.save

    redirect_to feed_path
  end

  def increment_deslike_feed
    @publication = Publication.all.find(params[:publication_deslike])

    @publication.deslike = @publication.deslike-1

    @publication.save

    redirect_to feed_path
  end

  def index
    if user_signed_in?
      if current_user.admin == false
        redirect_to feed_path
      end
    end

    @publications = Publication.all
  end

  def show
    if user_signed_in?
      if current_user.admin == false
        redirect_to feed_path
      end
    end
  end

  def new
    if user_signed_in?
      if current_user.admin == false
        redirect_to feed_path
      end
    end

    @users_admin = User.all.select { |u| u.admin == true }
    
    @categories = Category.all
    @publication = Publication.new
  end

  def edit
    if user_signed_in?
      if current_user.admin == false
        redirect_to feed_path
      end
    end
  end

  def create
    @publication = Publication.new(publication_params)
    @publication.user_id = current_user.id

    respond_to do |format|
      if @publication.save
        format.html { redirect_to feed_path, notice: 'Seu tópico foi criado com sucesso' }
        format.json { render :show, status: :created, location: @publication }
      else
        format.html { redirect_to feed_path, alert: 'Seu tópico não foi criado com sucesso porque a categoria seleciona não existe' }
        format.json { render json: @publication.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    if user_signed_in?
      if current_user.admin == false
        redirect_to feed_path
      end
    end
        
    respond_to do |format|
      if @publication.update(publication_params)
        format.html { redirect_to @publication, notice: 'Publication was successfully updated.' }
        format.json { render :show, status: :ok, location: @publication }
      else
        format.html { render :edit }
        format.json { render json: @publication.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @publication.destroy
    respond_to do |format|
      format.html { redirect_to feed_path, notice: 'Sua publicação foi deletada com sucesso' }
      format.json { head :no_content }
    end
  end

  private
    def set_publication
      @publication = Publication.find(params[:id])
    end

    def publication_params
      params.require(:publication).permit(:image, :title, :description, :category_id, :user_id)
    end
end
