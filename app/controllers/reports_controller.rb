class ReportsController < ApplicationController
  before_action :set_report, only: [:show, :edit, :update, :destroy]

  def index
    if user_signed_in?
      if current_user.admin == false
        redirect_to feed_path
      end
    end

    @reports = Report.all
  end

  def show
    if user_signed_in?
      if current_user.admin == false
        redirect_to feed_path
      end
    end
  end

  def new
    if user_signed_in?
      if current_user.admin == false
        redirect_to feed_path
      end
    end

    @report = Report.new
  end

  def edit
    if user_signed_in?
      if current_user.admin == false
        redirect_to feed_path
      end
    end
  end

  def create
    @report = Report.new(report_params)

    respond_to do |format|
      if @report.save
        format.html { redirect_to pessoas_path, notice: 'Você conseguiu efetuar o report com sucesso.' }
        format.json { render :show, status: :created, location: @report }
      else
        format.html { redirect_to pessoas_path, alert: 'Você não conseguiu efetuar o report com sucesso.' }
        format.json { render json: @report.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    if user_signed_in?
      if current_user.admin == false
        redirect_to feed_path
      end
    end

    respond_to do |format|
      if @report.update(report_params)
        format.html { redirect_to @report, notice: 'Report was successfully updated.' }
        format.json { render :show, status: :ok, location: @report }
      else
        format.html { render :edit }
        format.json { render json: @report.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    if user_signed_in?
      if current_user.admin == false
        redirect_to feed_path
      end
    end

    @report.destroy
    respond_to do |format|
      format.html { redirect_to reports_url, notice: 'Report was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    def set_report
      @report = Report.find(params[:id])
    end

    def report_params
      params.require(:report).permit(:motivation_id, :user_id, :description)
    end
end
