json.extract! motivation, :id, :name, :description, :created_at, :updated_at
json.url motivation_url(motivation, format: :json)
