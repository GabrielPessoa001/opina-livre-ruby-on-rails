class User < ApplicationRecord
  has_one_attached :image
  has_many :users
  has_many :publications

  devise :database_authenticatable, :registerable, :recoverable, :rememberable, :validatable
end
