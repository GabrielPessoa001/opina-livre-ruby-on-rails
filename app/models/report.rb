class Report < ApplicationRecord
  belongs_to :motivation
  belongs_to :user
end
