class Publication < ApplicationRecord
  has_one_attached :image
  after_initialize :init
  
  belongs_to :user
  belongs_to :category

  def init
    self.like  ||= 0
    self.deslike ||= 0
  end
end