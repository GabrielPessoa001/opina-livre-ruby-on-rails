require "application_system_test_case"

class MotivationsTest < ApplicationSystemTestCase
  setup do
    @motivation = motivations(:one)
  end

  test "visiting the index" do
    visit motivations_url
    assert_selector "h1", text: "Motivations"
  end

  test "creating a Motivation" do
    visit motivations_url
    click_on "New Motivation"

    fill_in "Description", with: @motivation.description
    fill_in "Name", with: @motivation.name
    click_on "Create Motivation"

    assert_text "Motivation was successfully created"
    click_on "Back"
  end

  test "updating a Motivation" do
    visit motivations_url
    click_on "Edit", match: :first

    fill_in "Description", with: @motivation.description
    fill_in "Name", with: @motivation.name
    click_on "Update Motivation"

    assert_text "Motivation was successfully updated"
    click_on "Back"
  end

  test "destroying a Motivation" do
    visit motivations_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Motivation was successfully destroyed"
  end
end
