Rails.application.routes.draw do
  devise_for :users
  devise_scope :user do
    get '/users/sign_out' => 'devise/sessions#destroy'
    get '/logout' => 'devise/sessions#destroy'

    get '/login' => 'devise/sessions#new'
  end
  
  root 'pages#dashboard'

  get '/feed', to: 'pages#feed'
  get '/geral', to: 'pages#geral'  
  get '/pessoas', to: 'pages#pessoas'

  get '/user/add_new_follower' => 'users#add_new_follower', :as => :add_new_follower
  get '/user/remote_follower' => 'users#remote_follower', :as => :remote_follower

  get '/increment_deslike_geral' => 'publications#increment_deslike_geral', :as => :increment_deslike_g
  get '/increment_like_geral' => 'publications#increment_like_geral', :as => :increment_like_g
  get '/increment_deslike_feed' => 'publications#increment_deslike_feed', :as => :increment_deslike_f
  get '/increment_like_feed' => 'publications#increment_like_feed', :as => :increment_like_f

  resources :reports
  resources :notices
  resources :users
  resources :publications
end