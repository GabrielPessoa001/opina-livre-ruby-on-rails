categories = Category.create([{ name: 'Esportes', description: 'Dados relacionados aos esportes físicos' },
                              { name: 'Esportes digitais', description: 'Dados relacionados aos esportes digitais' },
                              { name: 'Política', description: 'Política nacional ou internacional' },
                              { name: 'Economia', description: 'Dados relacionados à bolsa de velores ou aos vários campos da economia tanto local quanto internacional' }])