class CreateUsers < ActiveRecord::Migration[6.0]
  def change
    create_table :users do |t|
      t.string :name, unique: true
      t.integer :age
      t.text :description
      t.integer :phone
      t.string :address
      t.boolean :admin, default: false

      t.timestamps
    end
  end
end
