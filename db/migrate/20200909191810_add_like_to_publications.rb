class AddLikeToPublications < ActiveRecord::Migration[6.0]
  def change
    add_column :publications, :like, :integer
  end
end
