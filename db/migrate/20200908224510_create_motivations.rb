class CreateMotivations < ActiveRecord::Migration[6.0]
  def change
    create_table :motivations do |t|
      t.string :name
      t.text :description

      t.timestamps
    end
  end
end
