class AddPublicationToUser < ActiveRecord::Migration[6.0]
  def change
    add_reference :users, :publication, foreign_key: true
  end
end
