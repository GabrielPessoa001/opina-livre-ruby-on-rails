# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

puts '--------------------------------------------------------------------------------'

puts 'Criando todas as categorias do nosso sistema em ordem'

puts '============================================================>'

categories = Category.create([{ name: 'Esportes', description: 'Dados relacionados aos esportes físicos' },
                              { name: 'Esportes digitais', description: 'Dados relacionados aos esportes digitais' },
                              { name: 'Política', description: 'Política nacional ou internacional' },
                              { name: 'Música', description: 'Música nacional ou internacional' },
                              { name: 'Literatura', description: 'Literatura nacional ou internacional' },
                              { name: 'Educação', description: 'Educação pública ou particular' },
                              { name: 'Animais', description: 'Tudo que envolve o reino animal' },
                              { name: 'Matemática', description: 'Tudo sobre Matemática' },
                              { name: 'Xadrez', description: 'Xadrez nacional ou internacional' },
                              { name: 'Marvel', description: "Filmes, séries e HQ's exclusivas da Marvel" },
                              { name: 'DC', description: "Filmes, séries e HQ's exclusivas da DC" },
                              { name: 'Videogame', description: 'Jogos online ou offline' },
                              { name: 'RPG de mesa', description: 'RPG de mesa novos ou antigos' },
                              { name: 'Moba', description: 'Tudo sobre todos os jogos mobas' },
                              { name: 'Animes', description: 'Tudo sobre animes' },
                              { name: 'Séries', description: 'Tudo sobre séries' },
                              { name: 'Mangás', description: 'Tudo sobre mangás' },
                              { name: 'Religião', description: 'Religião no âmbito nacional ou internacional' },
                              { name: 'Cultura', description: 'Cultura nacional ou internacional' },
                              { name: 'Economia', description: 'Dados relacionados à bolsa de velores ou aos vários campos da economia tanto local quanto internacional' }])

puts '<============================================================'

puts 'Todas as cateogrias foram criadas com sucesso'

puts '--------------------------------------------------------------------------------'

puts 'Criando todos os tipos do nosso sistema em ordem'

puts '============================================================>'

kinds = Kind.create([{ name: 'Site' },
                          { name: 'Novos administradores' },
                          { name: 'Geral' },
                          { name: 'Novidades globais' },
                          { name: 'Urgente' },
                          { name: 'Informações adicionais' }])

puts '<============================================================'

puts 'Todas os tipos foram criados com sucesso'

puts '--------------------------------------------------------------------------------'

puts 'Criando todas as motivações do nosso sistema em ordem'

puts '============================================================>'

motivations = Motivation.create([{ name: 'Abuso verbal' },
                                { name: 'Tópicos inadequados' },
                                { name: 'Racismo' },
                                { name: 'Machismo' },
                                { name: 'Spam' },
                                { name: 'Usuário tóxico' },
                                { name: 'Hacker' }])

puts '<============================================================'

puts 'Todas as motivações foram criadas com sucesso'

puts '--------------------------------------------------------------------------------'